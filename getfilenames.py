# import the necessary modules we need
# in our case, blender's python API and python's os module
import bpy, os

# get the current selection
selection = bpy.context.selected_objects

# initialize a blank result variable
result = ""

# iterate through the selected objects
for sel in selection:
    # write the selected object's name and dimensions to a string
    result += "%s\n" % (sel.name)

# get path to render output (usually /tmp\)
tempFolder = os.path.abspath (bpy.context.scene.render.filepath)
# make a filename
filename = os.path.join (tempFolder, "list.txt")
# confirm path exists
os.makedirs(os.path.dirname(filename), exist_ok=True)
# open a file to write to
file = open(filename, "w")
# write the data to file
file.write(result)
# close the file
file.close()